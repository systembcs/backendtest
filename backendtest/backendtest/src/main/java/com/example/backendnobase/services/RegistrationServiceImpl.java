package com.example.backendnobase.services;

import com.example.backendnobase.exception.BaseException;
import com.example.backendnobase.exception.RegistrationException;
import com.example.backendnobase.model.TestModel;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Override
    public String registrationService(TestModel testModel) throws BaseException {
        if (testModel == null) {
            throw RegistrationException.requestNull();
        }
        //validate name
        if (Objects.isNull(testModel.getName()) || testModel.getName().isEmpty()) {
            throw RegistrationException.nameNull();
        }
        return "";
    }
}
