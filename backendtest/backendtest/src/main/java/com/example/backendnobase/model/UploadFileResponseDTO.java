package com.example.backendnobase.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class UploadFileResponseDTO {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private BigDecimal size;

    public UploadFileResponseDTO(String fileName, String fileDownloadUri, String fileType, BigDecimal size) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
    }

    // Getters and Setters (Omitted for brevity)
}