package com.example.backendnobase.exception;

public class RegistrationException extends BaseException {
    public RegistrationException(String errorCode) {
        super("registration : " + errorCode);
    }

    //request = null
    public static RegistrationException requestNull() {
        return new RegistrationException("request : null");
    }

    //name = null
    public static RegistrationException nameNull() {
        return new RegistrationException("name : null");
    }
}
