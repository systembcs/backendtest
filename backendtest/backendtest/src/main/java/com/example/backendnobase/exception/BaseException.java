package com.example.backendnobase.exception;

public abstract class BaseException extends Exception {
    public BaseException(String errorCode) {
        super(errorCode);
    }
}
