package com.example.backendnobase.controller;

import com.example.backendnobase.exception.BaseException;
import com.example.backendnobase.model.TestModel;
import com.example.backendnobase.model.UploadFileResponseDTO;
import com.example.backendnobase.services.FilesStorageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/test")
public class TestUploadFileController {
    @Autowired
    private FilesStorageServiceImpl filesStorageServiceImpl;

    @PostMapping("/upload")
    public UploadFileResponseDTO uploadFile(
            @RequestParam("text") String text,
            @RequestParam(value = "file", required = true) MultipartFile file
    ) throws BaseException {
        System.out.println("- text : "+text);
        System.out.println("- file : "+file.toString());

        UploadFileResponseDTO uploadFileResponseDTO = filesStorageServiceImpl.save(text,file);
        System.out.println("- uploadFileResponseDTO : "+uploadFileResponseDTO.toString());
        return uploadFileResponseDTO;
    }

    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponseDTO> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) throws BaseException{
        return Arrays.asList(files)
                .stream()
                .map(file -> {
                    try {
                        return uploadFileNoText(file);
                    } catch (BaseException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    public UploadFileResponseDTO uploadFileNoText(
            @RequestParam(value = "file", required = true) MultipartFile file
    ) throws BaseException {
        System.out.println("- file : "+file.toString());

        UploadFileResponseDTO uploadFileResponseDTO = filesStorageServiceImpl.save("text",file);
        System.out.println("- uploadFileResponseDTO : "+uploadFileResponseDTO.toString());
        return uploadFileResponseDTO;
    }
}
