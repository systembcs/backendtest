package com.example.backendnobase.model;

import lombok.Data;

@Data
public class TestModel {
    private String name;
    private String lastname;
}
