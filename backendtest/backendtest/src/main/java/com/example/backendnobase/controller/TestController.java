package com.example.backendnobase.controller;

import com.example.backendnobase.exception.BaseException;
import com.example.backendnobase.model.TestModel;
import com.example.backendnobase.services.RegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private RegistrationServiceImpl registrationService;

    @GetMapping("/get")
    public List<TestModel> testGet() {
        List<TestModel> testModelList = new ArrayList<>();
        TestModel testModel = new TestModel();
        testModel.setName("Test1");
        testModel.setLastname("LastName1");
        testModelList.add(testModel);

        testModel.setName("Test2");
        testModel.setLastname("LastName2");
        testModelList.add(testModel);
        return testModelList;
    }

    @PostMapping("/registration")
    public ResponseEntity<String> registration(@RequestBody TestModel requestTestModel) throws BaseException {
        String response = registrationService.registrationService(requestTestModel);
        return ResponseEntity.ok(response);
    }
}
