package com.example.backendnobase.services;

import com.example.backendnobase.exception.BaseException;
import com.example.backendnobase.model.UploadFileResponseDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface FilesStorageService {

    public UploadFileResponseDTO save(String fileName , MultipartFile file) throws BaseException;

    Resource loadFileAsResource(String fileName) throws BaseException ;

    public void deleteAll() throws BaseException ;

}
