package com.example.backendnobase.services;

import com.example.backendnobase.exception.BaseException;
import com.example.backendnobase.exception.FileUploadException;
import com.example.backendnobase.model.UploadFileResponseDTO;
import com.example.backendnobase.properties.FileStorageProperties;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Service
public class FilesStorageServiceImpl implements FilesStorageService {

    private Path fileStorageLocation = null;
    private static final long  MEGABYTE = 1024L * 1024L;


    public FilesStorageServiceImpl(FileStorageProperties fileStorageProperties) throws BaseException{
        System.out.println("!!!FilesStorageServiceImpl.fileStorageProperties : "+fileStorageProperties);
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();
        System.out.println("!!!FilesStorageServiceImpl.fileStorageLocation : "+fileStorageLocation);
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileUploadException("Could not create the directory where the uploaded files will be stored.");
        }
    }

    @Override
    public UploadFileResponseDTO save(String fileName, MultipartFile file) throws BaseException {
        if (!validateInput(fileName,file)) {
            throw FileUploadException.fileError();
        }
        //todo save file
        String fileNames = StringUtils.cleanPath(file.getOriginalFilename());
        System.out.println("File name : "+fileNames);
        try {
            // Check if the file's name contains invalid characters
            if(fileNames.contains("..")) {
                throw new FileUploadException("Sorry! Filename contains invalid path sequence " + fileNames);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            System.out.println("-Copy file to the target location ");
            System.out.println("-fileStorageLocation "+fileStorageLocation.toString());
            Path targetLocation = this.fileStorageLocation.resolve(fileNames);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/testUploadFile/")
                    .path(fileNames)
                    .toUriString();
            System.out.println("-fileDownloadUri "+fileDownloadUri);

            return new UploadFileResponseDTO(fileNames, fileDownloadUri, file.getContentType(), getFileSizeMB(file.getSize()));
        } catch (IOException ex) {
            throw new FileUploadException("Could not store file " + fileNames + ". Please try again!"+ ex.toString());
        }
    }

    @Override
    public Resource loadFileAsResource(String filename) {
    //
        return null;
    }

    @Override
    public void deleteAll() {
        //FileSystemUtils.deleteRecursively(root.toFile());
    }

    public boolean validateInput(String fileName, MultipartFile file) {
        System.out.println("!!! validateInput ");
        System.out.println("!!! file.getContentType() " + file.getContentType());

        boolean isValid = true;
        List<String> fileType_list = new ArrayList<>();
        fileType_list.add("image/jpeg");
        fileType_list.add("image/png");
        fileType_list.add("application/zip");
        fileType_list.add("application/x-7z-compressed");

        if (fileName.trim().length() < 0 || fileName == null) {
            System.out.println("!!! validateInput 1 ");
            return false;
        } else if (file == null) {
            System.out.println("!!! validateInput 2 ");
            return false;
        }
        String contentType = file.getContentType() == null ? "" : file.getContentType();
        System.out.println("!!!(fileName.trim().length() < 0 || fileName == null) : " +(fileName.trim().length() < 0 || fileName == null));
        System.out.println("!!! file == null : "+(file == null));
        System.out.println("!!! getFileSizeMB(file.getSize()) : "+getFileSizeMB(file.getSize()));
        System.out.println("!!! contentType : "+contentType);
        System.out.println("!!! fileType_list.contains(contentType) : "+fileType_list.contains(contentType));
        if (getFileSizeMB(file.getSize()).doubleValue() > 100.00) {
            System.out.println("!!! validateInput 3 ");
            isValid = false;
        } else if (!fileType_list.contains(contentType)) {
            System.out.println("!!! validateInput 4 ");
            isValid = false;
        }
        return isValid;
    }

    public BigDecimal getFileSizeMB(long bytes) {
        System.out.println("!!! bytes : "+bytes);
        System.out.println("!!! MEGABYTE : "+MEGABYTE);
        System.out.println("!!! bytes / MEGABYTE : "+(double)bytes / MEGABYTE);
        BigDecimal fileSizeMB = new BigDecimal((double)bytes / MEGABYTE).setScale(2, RoundingMode.HALF_UP);
        return fileSizeMB ;
    }
}
