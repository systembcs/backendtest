package com.example.backendnobase.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        System.out.println("### uploadDir : "+uploadDir);
        this.uploadDir = uploadDir;
    }
}
