package com.example.backendnobase.services;

import com.example.backendnobase.exception.BaseException;
import com.example.backendnobase.model.TestModel;

import java.io.IOException;

public interface RegistrationService {
    public String registrationService(TestModel testModel) throws IOException, BaseException;
}
