package com.example.backendnobase;

import com.example.backendnobase.properties.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})

public class BackendnobaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendnobaseApplication.class, args);
    }

}
